// console.log('hello world');

// [SECTION] Arithmetic Operators

	// +, -, *, /, %

	let numA = 5;
	let numB = 25;

	// Addition (+)
	let addition = numA + numB;
	console.log(addition);
	let minus = numA - numB;
	console.log(minus);
	let multiply = numA * numB;
	console.log(multiply);
	let divide = numB / numA;
	console.log(divide);
	let modulo = numB % numA;
	console.log(modulo);


	// Assignment Operator
		// Basic assignment Operator (=)
		let assignedNumber = 8;
		console.log(assignedNumber);


	// Assignment and Arithmetic Operators

		//  Addition Assignment Operator (+=)
			//  The addition assignment operator adds the value of the right operand to a variable and assigns the result of the addition operation to the variable.


		// long method
		// assignedNumber = assignedNumber + 2;
		// console.log("Result of the addition assignment Operator:" + assignedNumber)


		// short method
		assignedNumber += 2;
		console.log("Result of the addition assignment Operator:" + assignedNumber);


		// Subtraction Assignment Operator
		assignedNumber -=5;
		// assignedNumber = assignedNumber -5; - long method.
		console.log("Result of the subtraction assignment Operator:" + assignedNumber);

		// Multiplication Assignment Operator
		assignedNumber *=4;
		// assignedNumber - assignedNumber * 4; - long method
		console.log("Result of multiplication assignemnt Operator: "+ assignedNumber);


		// Division Assignment Operator
		assignedNumber/=20;
		// assignedNumber = assignedNumber / 20;
		console.log("Result of division assignment Operator" + assignedNumber);

	// Increment and Decrement
		let z = 1;
		// the value of "z" is added by a value of one before returning the value and storing it in the variable "increment"
		let increment = ++z;
		console.log("Result of pre-increment: " + increment);
		// The value of "z" was also increased eventhough we didn't implicitly specify any value reassignment
		console.log("Result of pre-increment: " +z);

		// the value of "z" is returned and stored in the variable "increment" then the value of "z" is increased by one.
		increment = z++;
		console.log("Result of post-increment: "+ increment);
		// this will result to 3
		console.log("Result of post-increment: "+ z);

		//  The value of "z" is decreased first by a value of 1 before returning and storing it in the "increment" variable; pre-decrement.
		let decrement = --z;
		console.log("Result of pre-decrement: "+decrement);
		console.log("Result of pre-decrement:" + z);
		// The value of z is 2 after pre-decrement

		// The value of "z" is returned and stored into the "decrement" variable before we subtract a value of one from the value of "z".
		decrement= z--;
		console.log("Result of post-decrement: "+ decrement);
		console.log("Result of post-decrement: "+ z)

	// Type Coercion
		/*
			-Type coercion is the automatic or implicit conversion of values from one data type to another.
		
		*/

	let numC= "10";
	let numD= 12;
	// adding/concatenating a string and a number will result to a string.
	let coercion = numC + numD;

	console.log(coercion);
	// typeof - shows the data type of a value.
	console.log(typeof coercion);

	let numE= 16;
	let numF= 14;

	// This is our regular operation which results to a value with a number data type.
	let nonCoercion = numE + numF;
	console.log(nonCoercion);
	console.log(typeof nonCoercion)


	// This will result to a number
	let numG= true +1;
	console.log(numG);


	numF = false + 1;
	console.log(numF);

	// Comparison Operators
	let juan ="juan";
		// Equality Operator (==)
			// - Checks whether the operands are equal or have the same content.
			// Returns a boolean value.


			console.log(1==1);
			console.log(1==2);
			console.log(1 == '1');
			console.log(0==false);
			console.log('juan' == 'juan');
			console.log('juan' == juan);

			// Inequality Operator
			/*

				- Checks whether the operands are noot equal or have different contents.
				- The "!" means not equal
			*/
			console.log("");
			console.log(1!=1);
			console.log(1!=2);
			console.log(1 != '1');
			console.log(0!=false);
			console.log('juan' != 'juan');
			console.log('juan' != juan);


			// Strict Equality Operator
				/*
					-Checks whether the operands are equal or have the same content and data types.
			

				*/
			console.log("");
			console.log(1===1);
			console.log(1===2);
			console.log(1 === '1');
			console.log(0===false);
			console.log('juan' === 'juan');
			console.log('juan' === juan);


			// Strict Inequality Operator

			/*
				- Checks whether the operands are not equal or have different contents.
				- It still compares the data type of 2 values.
			*/

			console.log("");
			console.log(1!==1);
			console.log(1!==2);
			console.log(1 !== '1');
			console.log(0!==false);
			console.log('juan' !== 'juan');
			console.log('juan' !== juan);

			// Relational Operators


				// Some comparison operators check whether one value is greater or less than to another value.

			let a= 50;
			let b= 65;

			// GT or Greater Than Operator(>)
			let isGreaterThan = a > b;
			// LT or Less Than Operator (<)
			let isLessThan = a < b;
			//GTE Greater Than or Equal Operator (>=)
			let isGTE = a >= b;
			// LTE Less Than or Equal Operator (<=)
			let isLTE = a <= b;

			console.log("Result of GT, LT, GTE and LTE: " );
			console.log(isGreaterThan);
			console.log(isLessThan);
			console.log(isGTE);
			console.log(isLTE);


			let numStr = "30";
			console.log(a > numStr); //true
			console.log(b <= numStr); //false

			let str ="thirty";
			
			console.log(b >= str); //false


			// Logical Operators

			let isLegalAge = true;
			let isRegistered = false;

			// Logical And Operator (&& double ampersand)
				// returns "true if all operands/conditions are true"
			let allRequirementsMet = isLegalAge && isRegistered;
			console.log("");
			console.log("Result of logical AND operator: " + allRequirementsMet);

			// Logical Or Operator(|| - Double pipe)
				// returns a true if at least one operand is true.
			let someRequirementsMet = isLegalAge || isRegistered;
			console.log("Result of logical OR operator: "+someRequirementsMet)

			// LogicalNot Operator (! - exclamation point)
				// returns the opposite value.
			let someRequirementsNotMet = !isRegistered;
			console.log("Result of logical Not Operator: "+someRequirementsNotMet);